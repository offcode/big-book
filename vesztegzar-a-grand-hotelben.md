Amikor Maud visszatért a szobájába, egy férfi lépett ki a szekrényből. Pizsamában, fején egy zöld selyem lámpaernyővel, és mosolygott, mintha minden rendben volna.

- Bocsánat - mondta, és mintha kalap volna, megemelte a lámpaernyőt -, nevem Van der Gullen Félix.

A leány csak most lett úrrá elzsibbadt idegein.
Az ajtóhoz hátrált.

- Mit akar itt?
Hogyan került a szekrényembe...?

- Kérem - mondta az idegen -, ne kiáltson, mert ezzel vesztemet okozná.
Amúgy is nyomban távozom.

- Hogy került a szekrénybe?

- A... szobából...

- Eh!
Nem így értem!

- Az ablakon keresztül jöttem.
Hajnalban.
Menekültem...
és a szálló elhagyott kertjében megpillantottam a nyitott ablakot.
Felhúzódzkodtam a párkányra, és benéztem...
Láttam, hogy az ágy üres...
Beugrottam, és a ruhásszekrénybe rejtőztem.
Sötét volt, puha és meleg.
Izgalmamban mélyen elaludtam.
Csak most ébredtem fel.

Mivel a nő nem felelt, az idegen letette a fejére hullott lámpaernyőt, meghajolt és elindult az ajtó felé.

---

- Megálljon! - kiáltotta Maud.
- Parancsoljon!
- felelte a rejtélyes ifjú, összecsapva papucsa sarkát.

- Csak nem akar...
egy szál pizsamában kilépni tőlem, a folyosóra?!!

Az idegen szolgálatkészen elindult az ablak felé.

Maud szinte felsikoltott:

- Megálljon!
- Kérem… - Kissé ijedten visszalépett, és úgy állt meg, mint aki minden parancsnak már előre is aláveti magát.

Maud kétségbeesetten nézett le a kertbe.

Érkező vendégekkel volt tele!

- Ha ide kilép pizsamában egy férfi az ablakomból, akkor én soha többé nem kerülhetek az emberek szeme elé...

- Csakugyan kínos látszat...
De talán utólag... félrehívhatná a vendégeket, és külön-külön megmagyarázná, hogy ki az, aki pizsamában kilépett innen...
- Hiszen én sem tudom!
- Az is igaz - hagyta rá nagy búsan Van der Gullen Félix.
- Akkor tehát maradok... - mondta, és leült egy karosszékbe.
- Nincs egy cigarettája véletlenül?
- Hogy képzeli? ...
Itt akar maradni?! ...
Így?!
A szobámban?!

Az idegen most már elkeseredetten csapott a combjára:

- Hát ne haragudjon, kérem, de ha egyszer nem mehetek ki sem az ajtón, sem az ablakon, akkor itt kell maradnom!
Azt még az én helyzetemben sem követelheti meg egy fiatalembertől, hogy légneművé változzék vagy felszívódjon, mint egy vakbéltünet!
...
Már bocsánatot kérek!

---
Kissé hangosabb lett, mivel felismerte helyzetének néhány pillanatnyi előnyét.

A nő riadtan nézett jobbra-balra.

- Na de hát... valamit mégiscsak tenni kell!
- Kérem, kisasszony, minden részvétem az öné.
Az emberek csakugyan hajlamosak arra, hogy egy pizsamás ember váratlan megjelenését félreértsék.
- Szégyellje magát!
- Ezért azok az emberek szégyenkezzenek, akik rosszhiszeműek...
Igazán nem ad egy cigarettát?

A nő odadobott egy dobozt az ágy melletti kis asztalkáról.
A férfi derűs arccal rágyújtott egy cigarettára, és a többit zsebre tette.

- Csacsi kis história - mondta azután, és kényelmesen hátradőlt a székben.

- Nem tudom még, hogy kicsoda, gonosztevő-e vagy őrült - mondta a nő dühösen -, de pusztán ezért a szörnyű helyzetért amibe belesodort, megérdemelné, hogy keresztüllőjem.

- Ez kihúzná a csávából.
De kissé radikális megoldás.
Kérem, ha ezzel menthetem a jó hírét és a hibámat, szívesen elveszem feleségül - ajánlotta udvariasan -, még mindig jobb, mint hogyha agyonlő.
Bár egyesek szerint ez nem bizonyos.

- Kérem...
- mondta a nő.
- Én ma este elutazom, addig maradjon itt, és lehetőleg éjszaka távozzon majd.
Megértett?
...
Arra is figyelmeztetem, hogy ha elhatározom magamat, akkor nem törődöm a jó híremmel, és átadom a szálló detektívjének.

- Igen, magától kitelik - felelte a férfi, mintha már régen ismerné Maudot.
- Hát majd igyekszem elnyerni a jóindulatát...
- Nem lesz alkalma rá, mert nyomban bezárom a fürdőszobába, és csak akkor bocsátom ki, ha elutazom...

---
Az ismeretlen arca siralmas kifejezésre ráncolódott.

- Képes volna egy kiszolgáltatott férfi helyzetével ennyire visszaélni? - kérdezte siralmasan az idegen.
- Ezt fogom tenni.
Választhat a szekrény és a fürdőszoba között.

- Bezár? ...
Kérem, fontolja meg.
Foglyul ejteni és rabságban tartani egy védtelen embert...

- Ha ilyen léhán fogja fel a helyzetet, még rosszabbul is járhat.
- Képes lenne verni? - kérdezte látszólag rémülten, és Maud bármilyen dühös is volt, elnevette magát.
- Legszívesebben rendőrnek adnám át, ezért bűnhődnie kellene!
Szigorúan! - folytatta azután még dühösebben.
- De úgy látszik, nem bánja egyáltalán, hogy egy úrinőt, a legelőkelőbb szállóban, ilyen helyzetbe hozott!
- Honnan tudja, hogy az én helyzetem nem rosszabb ezerszer?
Honnan tudja, mennyire szenvedek ebben a pillanatban?

---

Maud önkéntelenül is kissé ijedten lépett a férfi felé.

- Valami... baja van?
- Van...
Lehet, hogy perceken belül éhen halok...
- Síri hangon tette hozzá.
- Dél van, és én még nem ebédeltem...

- Nahát, tudja...
Azt sem csodálnám, ha körözött rabló lenne!
Ennyi cinizmus...

- Nézze, kedves zsarnokom, mielőtt megkezdem a büntetésem kitöltését a fürdőszobában tudja meg, hogy az ön magatartása is sok kívánnivalót hagy.

- Tessék?!
Hogy érti ezt?!

- Ön szemrehányást tesz, amiért ártatlanul meghurcoltam a világ előtt.
Viszont - és a hangja komor, sőt vádló lett: - hol volt ön ma éjszaka?!

---
- Mi?
- Jól hallotta a kérdést!
Miért másztam én éppen ebbe a szobába?
Mert üres volt.
És miért volt üres?
Mert ön nem tartózkodott benne.
Ha itt alszik az ágyban, akkor én diszkréten távozom a párkányról, és most nem fenyeget keserves rabság, egy fürdőszobában, ahol kénytelen leszek megenni egy pipereszappant, hogy elkerüljem az éhhalált...
Igen, kisasszony!
- és felemelte a hangját, mint egy zord közvádló a bűnper főtárgyalásán: - Hol volt ön ma hajnalban négy és öt óra között?

A nő döbbenten, csodálattal állt, a pizsamás ismeretlen káprázatos szemtelenségétől lenyűgözve...
Aztán így szólt:

- Mégsem zárom a fürdőszobába.
- Maradunk a szekrénynél?
- Nem!
  Átadom a szálló detektívjének.

---

- Ám legyen - felelte búsan, és mélyet sóhajtva elindult az ajtó felé, mint aki vérpadra lép.
- Várjon!
- kiáltotta Maud, és dühösen toppantott...
  Istenem, micsoda helyzet!
- Maradjon...!
- Nem, nem...
Ön megsértett.

Maud a karjába kapaszkodott:

- Csak tréfáltam...
Szó sem lehet arról, hogy így kilépjen a szobámból...

Az idegen sóhajtott, és hősi komorsággal megrázta a fejét:

- Nem...
Mégis megyek a detektívhez.
Önként!
Az ilyen ember bűnhődjön...
Igaza volt.
- És tragikusan a mellére csapott kétszer.
- Én bűnhődni akarok!
Mea culpa!
- kiáltott.
- Mea culpa...
én bűnhődni akarok.

Maud megragadta a karját:

- Az istenért, ne kiabáljon!

- De én nem hagyom magam a fürdőszobába zárni...

- Nem zárom be...
- suttogta megtörten a nő.

- Az más...
Akkor esetleg maradok - és visszaült.
- Nem vagyok rossz ember, ha arról van szó, hogy egy hölgy a segítségemet kéri...

Ebben a pillanatban kopogtak az ajtón.
