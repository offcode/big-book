const readline = require('readline')
_ = require('lodash')

chunks = (xs, n) => {
    const len = Math.floor(xs.length / n) + 1
    const count = xs.length % n
    const longer = _.take(xs, count * len)
    const chunkedLonger = _.chunk(longer, len)
    const shorter = _.drop(xs, count * len)
    const chunkedShorter = _.chunk(shorter, len-1)
    return _.concat(chunkedLonger, chunkedShorter)
}

remix = (lines, n) => {
    const mixed = _.shuffle(chunks(lines, n))
    return _.flatten(mixed)
}

if (process.argv[2] == 'test') {
    input = [1,2,3,4,5]
    wanted = [[1,2], [3,4], [5]]
    got = chunks(input, 3)
    console.assert(_.isEqual(wanted, got), "%s != %s", JSON.stringify(wanted), JSON.stringify(got))
    result = remix(input, 3)
    console.log(result)
}
else {
    lines = []
    const rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout,
        terminal: false
    })
    rl.on('line', line => {
        lines.push(line)
    })
        .on('close', () => {
            n = parseInt(process.argv[2])
            remix(lines, n).forEach(item => {
                console.log(item)
            })
        })
}
