# Az idegen a szobában

## Rejtő Jenő: Vesztegzár a Grand Hotelben

A nagytiszteletű úr éppen közepén volt a tósztnak...
A pezsgőspohár habja végigcsorgott kövér ujjain.
A díszes vendégkoszorú minden tagja koccintásra készen tartotta kezét a poharak karcsú fogantyúján.
Még tartott a tószt és egyre nagyobb magasságokba ívelt.
A háttérben egy egyiptomi zenekar állt, vezetőjük pálcája a levegőben, hogy amint a tósztnak vége szakad, rögtön tust húzhassanak.
Ekkor a család háziorvosa nesztelen léptekkel bejött a terembe, s pár szót súgott az asztalfőn elnöklő asszonyság fülébe, mire az hirtelen felállt és távozott.

---
Amikor Maud visszatért a szobába, egy úr lépett ki a szekrényből, pizsamában, fején egy ízléses zöld selyem lámpaernyővel, és barátságosan mosolygott.

- Bocsánat - mondta, és udvariasan megemelte a lámpaernyőt -, nevem Van der Gullen Félix.

A leány csak most lett úrrá elzsibbadt idegein. Az ajtóhoz hátrált. Az ismeretlen bizonyára őrült.

---

A tószt azonban, mint a kilőtt gránát, nem hagyta magát ez incidens által reptében megzavartatni, hanem tovább ívelt felfelé.

"...e dicső férfiút, aki országunk terhét vállán viseli, a mi táborunk legfőbb vezérét, ki bár jelenleg nincs is jelen, az Isten számos és számtalan évig éltesse!"

A végszavakat elmosta a pohárcsörgés, az éljenzés és a tus.
A vendégek fellelkesültek, hogy a széküket hátrarúgták és a falhoz vágták a poharukat.
Könnyes szemmel ölelkeztek össze.

---
- Mit akar itt? Hogyan került a szekrényembe...?

- Kérem önt - mondta az őrült idegen -, ne kiáltson, mert ezzel vesztét okozná egy jólelkű, vidám embernek, aki amúgy is nyomban távozik.

- Hogy került a szekrénybe?

- A... szobából...

- Eh! Nem így értem!

- Az ablakon keresztül jöttem. Hajnalban. Menekültem... és a szálló elhagyott kertjében megpillantottam a nyitott ablakot. 

---

"Éljen! Ezer esztendeig éljen!"

"Nagyságos és tekintetes, vitézlő Baradlay Kazimir úr, ura nagy földeknek, falvaknak és városoknak, szíveknek és véleményeknek … maga a Dalai Láma."

Különféle rendű és rangú uraságok gyűltek össze erre az ünnepélyes nara, hogy megvitassák és elfogadják azt a programot, mely évszázadokra meghatározza a nemzetek sorsát.

Ennek a nagyszabású értekezletnek zárójelenete a mostani díszlakoma, melyet Baradlay úr fényes kastélyterme szolgáltat.
Kár, hogy az ünnep hőse nem lehet rajta jelen.

A díszlakománál az úrnő elnököl helyette.
Az értekezletnél pedig az adminisztrátor helyettesítette, akit Rideghváry Bencének hívták.

---

- Felhúzódzkodtam a párkányra, és benéztem... Láttam, hogy az ágy üres... Nosza, beugrottam, a szekrénybe rejtőztem, és izgalmamban mélyen elaludtam. Csak most ébredtem fel. Közben egy sportkabát akasztójánál fogva leszakadt, ezért elnézését kérem...

Mivel a nő nem felelt, az idegen letette a fejére hullott lámpaernyőt, meghajolt és elindult az ajtó felé.

- Megálljon! - kiáltotta Maud.

- Parancsoljon! - felelte a rejtélyes ifjú, összecsapva papucsa sarkát.

- Csak nem akar... egy szál pizsamában kilépni tőlem, a folyosóra?!!

Az idegen szolgálatkészen elindult az ablak felé.

Maud szinte felsikoltott:

- Megálljon!

- Kérem... - Kissé ijedten visszalépett, és úgy állt meg, mint aki minden parancsnak már előre is aláveti magát. Maud kétségbeesetten nézett le a kertbe.

Érkező vendégekkel volt tele!

---

A tószt végén észrevették, hogy az elnöklő háziasszony hiányzik, hiába koccintanának vele.

Az üres szék mögött álló komornyik felvilágosította az uraságokat, hogy az orvos volt itt, az súgott neki valamit, mire a nagyságos asszony eltávozott.
Valószínűleg őnagysága hívatta magához.

---

- Ha ide kilép pizsamában egy férfi az ablakomból, akkor én soha többé nem kerülhetek az emberek szeme elé...
- Csakugyan kínos látszat... De talán utólag... félrehívhatná a vendégeket, és külön-külön megmagyarázná, hogy ki az, aki pizsamában kilépett innen...
- Hiszen én sem tudom!
- Az is igaz - hagyta rá nagy búsan Van der Gullen Félix. - Akkor tehát maradok... - mondta, és leült egy karosszékbe. - Nincs egy cigarettája véletlenül?
- Hogy képzeli?... Itt akar maradni?!... Így?! A szobámban?!

---

Egypár érzékeny kedély elejti azt a kérdést, vajon mi baja lehet őnagyságának.
Mire az adminisztrátor úr, ki az üres elnöki szék mellett jobbról foglalt helyet, sietett az egész társaságot megnyugtatni, hogy Baradlayt ismét a "szokott betegsége" vette elő.

Egypár avatottabb vendég aztán halkan elmondta kevésbé tudós szomszédjának a nyilvános titkot, hogy Baradlay Kazimir szívérelmeszesedésben szenved már évtizedek óta, s emiatt gyakran nehéz szívbántalmak veszik elő; de amik mellett még azért évtizedekig élhet tovább, rendes életmód mellett.

---

Az idegen most már elkeseredetten csapott a combjára:

- Hát ne haragudjon, kérem, de ha egyszer nem mehetek ki sem az ajtón, sem az ablakon, akkor itt kell maradnom! Azt még az én helyzetemben sem követelheti meg egy fiatalembertől, hogy légneművé változzék vagy felszívódjon, mint egy vakbéltünet!... Már bocsánatot kérek!

Kissé hangosabb lett, mivel felismerte helyzetének néhány pillanatnyi előnyét.

---

A nagytiszteletű úr mindjárt emlékezett is egy történetre, melyben valami angol orvos, aki hasonló érelmeszesedésben szenvedett, évekkel előre megjósolta saját halála óráját.
Mire a történet eljutott az asztal végéig, addigra belekeveredett Józsa Gyuri egyik kalandja is.

Hiszen csak szokott baja a nagyságos úrnak, kit az Úristen éltessen még ezer esztendeig.

---

A nő riadtan nézett jobbra-balra.

- Na de hát... valamit mégiscsak tenni kell!...

---

A háziorvos pedig ezt a néhány szót súgta az elnöklő úrhölgy fülébe: "Még hatvan perc!”

---

- Kérem, kisasszony, minden részvétem az öné. Az emberek csakugyan hajlamosak arra, hogy egy pizsamás ember váratlan megjelenését félreértsék.
- Szégyellje magát!
- Ezért azok az emberek szégyenkezzenek, akik rosszhiszeműek... Igazán nem ad egy cigarettát?

A nő odadobott egy dobozt az ágy melletti kis asztalkáról. A férfi derűs arccal rágyújtott egy cigarettára, és a többit zsebre tette.

- Csacsi kis história - mondta azután, és kényelmesen hátradőlt a székben.

- Nem tudom még, hogy kicsoda, gonosztevő-e vagy őrült - mondta a nő dühösen -, de pusztán ezért a szörnyű helyzetért amibe belesodort, megérdemelné, hogy keresztüllőjem.

- Ez kihúzná a csávából. De kissé radikális megoldás. Kérem, ha ezzel menthetem a jó hírét és a hibámat, szívesen elveszem feleségül - ajánlotta udvariasan -, még mindig jobb, mint hogyha agyonlő. Bár egyesek szerint ez nem bizonyos.

- Kérem... - mondta a nő. - Én ma este elutazom, addig maradjon itt, és lehetőleg éjszaka távozzon majd. Megértett?... Arra is figyelmeztetem, hogy ha elhatározom magamat, akkor nem törődöm a jó híremmel, és átadom a szálló detektívjének.

- Igen, magától kitelik - felelte a férfi, mintha már régen ismerné Maudot. - Hát majd igyekszem elnyerni a jóindulatát...

- Nem lesz alkalma rá, mert nyomban bezárom a fürdőszobába, és csak akkor bocsátom ki, ha elutazom...

Az ismeretlen arca siralmas kifejezésre ráncolódott.

- Képes volna egy kiszolgáltatott férfi helyzetével ennyire visszaélni? - kérdezte siralmasan az idegen.

- Ezt fogom tenni. Választhat a szekrény és a fürdőszoba között.

- Bezár?... Kérem, fontolja meg. Foglyul ejteni és rabságban tartani egy védtelen embert...

- Ha ilyen léhán fogja fel a helyzetet, még rosszabbul is járhat.

- Képes lenne verni? - kérdezte látszólag rémülten, és Maud bármilyen dühös is volt, elnevette magát.

- Legszívesebben rendőrnek adnám át, ezért bűnhődnie kellene! Szigorúan! - folytatta azután még dühösebben. - De úgy látszik, nem bánja egyáltalán, hogy egy úrinőt, a legelőkelőbb szállóban, ilyen helyzetbe hozott!

- Honnan tudja, hogy az én helyzetem nem rosszabb ezerszer? Honnan tudja, mennyire szenvedek ebben a pillanatban?

Maud önkéntelenül is kissé ijedten lépett a férfi felé.

- Valami... baja van?

- Van... Lehet, hogy perceken belül éhen halok... - Síri hangon tette hozzá. - Dél van, és én még nem ebédeltem...

- Nahát, tudja... Azt sem csodálnám, ha körözött rabló lenne! Ennyi cinizmus...

- Nézze, kedves zsarnokom, mielőtt megkezdem a büntetésem kitöltését a fürdőszobában tudja meg, hogy az ön magatartása is sok kívánnivalót hagy.

- Tessék?! Hogy érti ezt?!

- Ön szemrehányást tesz, amiért ártatlanul meghurcoltam a világ előtt. Viszont - és a hangja komor, sőt vádló lett: - hol volt ön ma éjszaka?!

- Mi?...

- Jól hallotta a kérdést! Miért másztam én éppen ebbe a szobába? Mert üres volt. És miért volt üres? Mert ön nem tartózkodott benne. Ha itt alszik az ágyban, akkor én diszkréten távozom a párkányról, és most nem fenyeget keserves rabság, egy fürdőszobában, ahol kénytelen leszek megenni egy pipereszappant, hogy elkerüljem az éhhalált... Igen, kisasszony! - és felemelte a hangját, mint egy zord közvádló a bűnper főtárgyalásán: - Hol volt ön ma hajnalban négy és öt óra között?

A nő döbbenten, csodálattal állt, a pizsamás ismeretlen káprázatos szemtelenségétől lenyűgözve... Aztán így szólt:

- Mégsem zárom a fürdőszobába.

- Maradunk a szekrénynél?

- Nem! Átadom a szálló detektívjének.

- Ám legyen - felelte búsan, és mélyet sóhajtva elindult az ajtó felé, mint aki vérpadra lép.

- Várjon! - kiáltotta Maud, és dühösen toppantott... Istenem, micsoda helyzet! - Maradjon...!

- Nem, nem... Ön megsértett.

Maud a karjába kapaszkodott:

- Csak tréfáltam... Szó sem lehet arról, hogy így kilépjen a szobámból...

Az idegen sóhajtott, és hősi komorsággal megrázta a fejét:

- Nem... Mégis megyek a detektívhez. Önként! Az ilyen ember bűnhődjön... Igaza volt. - És tragikusan a mellére csapott kétszer. - Én bűnhődni akarok!... Mea culpa! - kiáltott. - Mea culpa... én bűnhődni akarok.

Maud megragadta a karját:

- Az istenért, ne kiabáljon!

- De én nem hagyom magam a fürdőszobába zárni...

- Nem zárom be... - suttogta megtörten a nő.

- Az más... Akkor esetleg maradok - és visszaült. - Nem vagyok rossz ember, ha arról van szó, hogy egy hölgy a segítségemet kéri...

Ebben a pillanatban kopogtak az ajtón.
