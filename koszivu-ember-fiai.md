HATVAN PERC!

A nagytiszteletű úr éppen közepén volt a tósztnak...
A pezsgőspohár habja végigcsorgott kövér ujjain.
A díszes vendégkoszorú minden tagja koccintásra készen tartotta kezét.
Még mindig tartott a tószt és csak ívelt egyre magasabbra.
A háttérben az alkalomra bérelt zenekar rajtra készen állt, hogy amint a tósztnak vége, máris húzzák a tust.
Ekkor a család háziorvosa surrant be a terembe nesztelenül.
Pár szót súgott az asztalfőn ülő asszonyság fülébe.
Erre az hirtelen felállt és távozott.

A tószt azonban rá sem hederített.
Nem hagyta, hogy ez a kis incidens megzavarja, hanem tovább ívelt felfelé.

"...e dicső férfiút, aki országunk terhét vállán viseli, a mi táborunk legeslegfőbb vezérét, ki bár jelenleg nincs jelen, az Isten számos és számtalan évig éltesse!"

A végszavakat elmosta a pohárcsörgés, az éljenzés és a tus.
A vendégek fellelkesültek, hogy a széküket hátrarúgták és a falhoz vágták a poharukat.
A meghatottságtól zokogva ölelkeztek össze.

"Éljen! Ezer esztendeig éljen!"

"Nagyságos és tekintetes, vitézlő Baradlay Kazimir úr, ura nagy földeknek, falvaknak és városoknak, szíveknek és véleményeknek … maga a földreszállt Dalai Láma."

Különféle rendű és rangú uraságok gyűltek össze erre az ünnepélyes napra, hogy megvitassák és elfogadják azt a programot, mely évszázadokra meghatározza a nemzetek sorsát.

Ennek a nagyszabású értekezletnek zárójelenete a mostani díszlakoma, melyet Baradlay úr fényes kastélyterme szolgáltat.
Kár, hogy az ünnep hőse nem lehet rajta jelen.

A díszlakománál az úrnő elnököl helyette.

Az értekezletnél pedig az adminisztrátor, Rideghváry Bence helyettesítette.

A tószt végén észrevették, hogy az elnöklő háziasszony hiányzik, hiába koccintanának vele.

Az üres szék mögött álló komornyik felvilágosította az uraságokat, hogy az orvos volt itt, az súgott neki valamit, mire a nagyságos asszony eltávozott.
Valószínűleg őnagysága hívatta magához.

Egypár érzékeny kedély elejti azt a kérdést, vajon mi baja lehet őnagyságának.
Mire az adminisztrátor úr, ki az üres elnöki szék mellett jobbról foglalt helyet, sietett az egész társaságot megnyugtatni, hogy Baradlayt ismét a "szokott betegsége" vette elő.

Egypár avatottabb vendég aztán halkan elmondta kevésbé tudós szomszédjának a nyilvános titkot, hogy Baradlay Kazimir szívérelmeszesedésben szenved már évtizedek óta, s emiatt gyakran nehéz szívbántalmak veszik elő; de amik mellett még azért évtizedekig élhet tovább, rendes életmód mellett.

A nagytiszteletű úr mindjárt emlékezett is egy történetre, melyben valami angol orvos, aki hasonló érelmeszesedésben szenvedett, évekkel előre megjósolta saját halála óráját.
Mire a történet eljutott az asztal végéig, addigra belekeveredett Józsa Gyuri egyik kalandja is.

Hiszen csak szokott baja a nagyságos úrnak, kit az Úristen éltessen még ezer esztendeig.

A háziorvos pedig ezt a néhány szót súgta az elnöklő úrhölgy fülébe: "Még hatvan perc!"

---

Egy úrhölgynél a sápadt arc szakmai elvárás.
Jól jön ilyenkor, amikor egy gyakorlatlanabb nő elsápadna a hír hallatán.

Átlépett az ajtón a mellékterembe, megragadta az orvos kezét, és azt kérdezte:
- Ezt komolyan mondta?

Az orvos szigorú arccal bólintott.

Mikor aztán még egy üres szoba ajtaja zárult be köztük s a vidám társaság között, akkor ismétlé az elébbi mondatot:

- Csak hatvan perce van még élni.
Mindenkit eltávolított maga mellől.
Csak nagyságodat óhajtotta.
Rám semmi szükség többé.

A harmadik ajtónál az orvos aztán hátramaradt, s engedte az úrnőt egyedül menni.

A negyedik szobában két nagy aranyrámában egymás mellett állt a főúri pár életnagyságú képe mint vőlegény és menyasszony.
A márványarcú hölgy, midőn e két kép előtt elhaladt, arcát két kezébe temette.
Könny, zokogás akart kitörni; azt kellett visszafojtania.
Ez a harc elrabolt a hatvan percből - egy felet.
S azt a fél percet fel fogják neki panaszolni!

Azután még egy üres, siket szoba következett, azután nyílt amaz ajtó, melyen belül ott feküdt az a férfi, akinek a szívere kővé vált.

Feje magasan felpócolva vánkosai között, minden vonása rendbe szedve, mintha modellképpen ülne a halálnak.

A nő odasietett hozzá.

- Vártam önre - neheztelt a férfi.
- Rögtön jöttem - szabadkozott a nő.
- Közben megállt sírni.
Pedig tudja, hogy az én időm rövid.

A nő összeszorította ökleit és ajkait.

- Semmi gyöngeség, Marie! - szólt egyre hidegebb hangon a férj.
- Ez a természet rendje.
Én hatvan perc múlva tehetetlen tömeg leszek.
Orvosi papírom van róla.
A vendégek jól mulatnak?

A nő némán intett.

- Csak mulassanak tovább.
Ne engedje őket szétszéledni.
Ahogy összejöttek az értekezletre, maradjanak együtt a torra.
A gyászszerszartást már rég előkészítettem.
A két koporsó közül a fekete márványt fogja ön számomra választani.
A gyászénekkart a debreceni iskola adja.
Csak régi zsoltárénekeket tudnak úgyis.
A sírbeszédről gondoskodtam.
Persze nem én mondom, hanem akit megbíztam vele.
Utána a helybeli pap egy egyszerű miatyánkot mond el a sírbolt előtt, semmi mást.
Eddig érthető?

A nő csak bámult maga elé.

- Kérem, Marie.
Amit most mondok, azt nem fogom tudni elismételni.
Legyen olyan jó, üljön ide az ágyam mellé a kisasztalhoz, van rajta toll, papír.
Írja le, amit mondtam és még mondani fogok.

A nő híven teljesítette a parancsot, leült az ágy melletti kisasztalhoz, és leírta az eddig hallottakat.

A férj folytatta:

- Ön hű és engedelmes nő volt egész életében, Marie.
Minden szavamat teljesítette.
Még egy óráig leszek önnek az ura.
De amit ebben az órában önnek megszabok, az ki fogja tölteniaz egész életét.
Még halálom után is ura leszek önnek.
Ura, parancsolója, kőszívű zsarnoka.
- Ah, hogy fojt valami!
Adjon hat cseppet abból a digitalinból...

A nő kis aranykanálkában beadta neki a gyógyszert.
A beteg ismét könnyebben beszélt:

- Írja le, hogy utoljára hogy rendelkezem!
Ezt a feljegyzést önön kívül ne lássa senki más.
Egy nagy művet alkottam, melynek nem szabad velem együtt összeroskadni.
A föld ne mozogjon, hanem álljon.
S ha az egész föld előremegy is, ez a darab föld, ami a mienk, ne menjen vele.
Most a tengelye esik ki a gépnek.
De van három fiam, ki fölvált, midőn én porrá leszek.
Írja, Marie, mi dolga lesz a fiaimnak halálom után.
- Előbb adjon egyet abból a gyógyszerből.

- Három fiam mind nagyon fiatal még arra, hogy helyemet elfoglalhassa.
Előbb járják ki az élet iskoláját.
Addig ön nem találkozhat velük.
Ne sóhajtozzon, Marie, nagy gyerekek már.

Legidősebb fiam, Ödön, maradjon a szentpétervári udvarnál.
Most még csak követségi titkár, idővel magasabbra lesz hivatva.
Az a hely jó iskola lesz neki.
A természet sok rajongást oltott szívébe, ami fajunkat nem idvezíti.
Ott az orosz udvarnál kigyógyítják mindabból.
Ott megtanulja ismerni a különbséget az emberek között, akik jogosan születnek - és jogtalanul világra jönnek.
Ott megtanulja, hogyan kell a magasban megállni és a földre le nem szédülni.
Ott megismeri, mi értéke van egy nőnek és egy férfinak egymás ellenében.
Ott majd elfásul a rajongása, s midőn visszatér, mint egész ember veheti kezébe a kormányrudat, amit én elbocsátok kezemből.
Adjon ön mindig elég pénzt neki, hogy versenyezhessen az orosz udvar nemes ifjaival.
Nézze el túlcsapongó szeszélyeit, ezeket át kell élni annak, ki a közöny magaslatára akar jutni.

A beszélő az órára tekinte.
Oly sok mondanivaló van még.

- Azt a leányt - folytatá halkan a beteg -, ki miatt el kellett neki hagynia e házat, igyekezzék ön férjhez adni.
Ne kíméljen semmi költséget.
Ha makacsul megmaradna e némber határozatánál, járjon ön utána, hogy a leány atyja áttétessék Erdélybe.
Ott sok összeköttetésünk van.
Ödön kinn maradjon mindaddig, míg ők innen el nem távoznak, vagy Ödön odakünn meg nem házasodik.

A nő csendesen írta a rábízottakat.

- Második fiam, Richárd, még egy évig marad a királyi testőrségnél.
De ez nem életpálya.
Innen lépjen át a lovassághoz; ott szolgáljon ismét egy évig, s akkor igyekezzék a táborkarba bejutni.
Ügyesség, vitézség és hűség három nagy lépcső a magasba jutáshoz.
Mind a hármat a gyakorlat hozza meg.
Ott egy egész meghódítatlan régió áll előttünk.
Az oktalan dölyf nem engedi.
Az én fiam törjön utat mások előtt.
Lesz Európának háborúja, ha egyszer megkezdődik a földindulás.
Egy Baradlay Richárd számára azokban sok végeznivaló lesz.
Dicsősége mindnyájunkra fog világítani!
Richárd ne nősüljön soha.
Az asszony csak útjában volna neki.
Az ő feladata legyen: emelni testvéreit.
Milyen dicső ajánlólevél egy testvér, ki a csatában elesett!

Marie, ön nem ír?
Kérem, Marie, ne legyen most gyönge; csak negyven perc még az idő, s nekem annyi mondanivalóm van még.

A nő nem merte mutatni fájdalmát, és írt csendesen.

- Harmadik fiam, a legifjabb, Jenő: az én kedvencem.
Nem tagadom, hogy a legjobban szerettem őt mind a három között.
Ő nem fogja azt soha tudni.
Mert hiszen úgy bántam vele, mintha mostohája volnék.
Tovább is úgy bánjék ön vele.
Maradjon Bécsben, és szolgáljon a hivatalban, és tanulja magát fokrul fokra felküzdeni.
Ez a küzdelem neveli őt simának, okosnak és eszesnek.
Legyen mindig kényszerítve arra, hogy kedvében járjon azoknak, akiket ismét mint lépcsőket fog felhasználni, hogy magasabbra emelkedjék.
Nem kell őt hazulról kényeztetni, hogy tanulja meg felhasználni az idegent és minden embernek mérlegelni az értékét.
Ápolni kell benne a nagyravágyást; fenntartani és felkerestetni vele az ismeretséget nagy nevekkel és hatalmas befolyásokkal, mik családi összeköttetésre vezethetnek - a költői ábrándok követése nélkül.

Egy pillanatnyi eltorzulása ez arcnak tanúsítá, mily végtelen kínokat kell kiállnia.
Csak pillanatig tartott az.
A nemes akarat legyőzte a pór természetet.

- Ily három erős oszlop fönn fogja tartani a művemet.
Egy diplomata - egy katona - egy főhivatalnok.
Miért nem építhettem még tovább, amíg ők megerősödnek!
Marie!
Nőm!
Baradlayné asszony!
Én kérem, én kényszerítem, én megidézem önt, hogy cselekedje azt, amit önnek rendelek.
Ezt a hideg verítéket homlokomon nem a halálküzdelem izzasztja, hanem a rettegés, hogy hasztalan fáradtam.
Egy negyedszázad munkája odavesz.
Álmodozó rajongók tűzbe viszik a gyémántot, s nem tudják, hogy az ott satnya elemeire fog feloszlani, s soha vissza nem jegecül.
Ez a mi nyolcszázados gyémánt nemességünk!
Ez az örökké éltetője az egész nemzetnek.
- Ah, Marie, ha tudná, ez az én kőszívem mit szenved!
Nem.
Ne gyógyszert adjon.
Az nem használ ennek.
Tartsa elém fiaim arcképeit.

A nő odavitte a hármas tokba foglalt három miniatűr képet.
A kőszívű ember sorba nézte őket, s azalatt elcsillapultak fájdalmai.
Száraz csontujjával a legidősebb arcára mutatva suttogá:

- Úgy hiszem, ez legjobban hasonlíthatna hozzám.

Azután félretolta maga elől az arcképeket, s folytatta hidegen:

- Semmi érzelgés, az idő rövid.
Én nemsokára megtérek atyáimhoz, itt hagyom fiaimnak, mit őseim rám hagytak.
"Nemesdomb" nem esik ki a történelemből.
Központja, tűzhelye, napja lesz ez a bolygó elvnek.
Ön itt marad utánam.

A hölgy megállt az írás közben, és bámulva tekinte a beszélőre.

- Ön kételkedve néz rám.
Egy nő, egy özvegy mit tehetne azon munkában, melyben egy férfi összeroskadt?
Megmondom azt.
Hat héttel halálom után ön férjhez fog menni.

A nő kiejté kezéből a tollat.

- Ön Rideghváry Bencének fogja nyújtani kezét.

Itt a nő nem bírt magával többé, odarohant férje ágya mellé, és annak kezét megragadva, a legforróbb könnyekkel áztatá azt.

A kőszívű ember lehunyta szemeit, és tanácsot kért a sötétségtől.

- Marie, hagyja ön abba!
Most nincs idő az ön sírására.
Ön fiatal még; nincs negyvenéves.
Ön szép, és örökké az marad.
Huszonnégy év előtt, mikor nőül vettem önt, sem láttam önt szebbnek, mint most.
Hollófekete haja, ragyogó szemei voltak - most is azok.
Szelíd és szemérmes volt ön; most sem szűnt meg az lenni.
Én nagyon szerettem önt.
Hiszen tudja azt jól.
Az első évben született legidősb fiam, Ödön, a másodikban második fiam, Richárd, a harmadikban a legifjabb, Jenő.
Akkor egy súlyos betegséggel látogatott meg Isten, amiből mint nyomorék támadtam föl.
Az orvosok megmondták, hogy a halálé vagyok.
Egyetlen csók, amit az ön édes ajka ad nekem, meg fog ölni.
És én itt haldoklom az ön oldala mellett már húsz év óta, elítélve, kiszentenciázva.
És én tudtam élni, mert volt egy nagy eszme, ami élni kényszerített, tartogatni egy kínokkal és lemondásokkal folytatott életet.
Ó, milyen életet!
Örök tagadását mindennek, ami öröm, ami érzés, ami gyönyörűség!
Lemondtam mindenről, amiért emberi szív dobog.
Lettem rideg, kiszámító, hozzájárulhatatlan.
Csak a jövendőnek éltem.
Egy oly jövendőnek, mely nem egyéb, mint a múlt örökkévalósága.
Ebben neveltem mind a három fiamat.
Ebben örökítettem meg nevemet.
E néven a jelenkor átka fekszik, de a jövendő áldása ragyog.
E névért szenvedett ön oly sokat, Marie.
Önnek még boldognak kell lenni az életben.

A nő zokogása tiltakozott e szó ellen.

- Én úgy akarom - szólt a férfi, és elrántotta tőle kezét.
- Térjen ön az asztalhoz vissza, és írja.
Végakaratom ez.
Nőm hat hét múlva halálom után adja kezét Rideghváry Bencének, ki nyomdokomba lépni leginkább érdemes.
Így leend nyugtom a föld alatt és üdvöm az égben.
Leírta-e ön, amit eléje mondtam, Marie?

A nő kezéből kihullott a toll és hallgatott.

- Az óra végire jár - rebegé a haldokló.
- De "non omnis moriar"!
A mű, mit megkezdettem, utánam fennmarad, Marie!
Tegye ön kezemre kezét, és hagyja rajta, míg azt megfagyni érzi.
Semmi érzelgés, semmi könny.
Nem veszünk búcsút.
Lelkemet önre hagytam, s az nem fogja önt elhagyni soha; az számon kérendi öntül minden reggel, minden este, hogyan töltötte ön be azt, amit önnek végső órámban meghagytam.
Én mindig itt leszek!

A nő úgy reszketett.

A haldokló pedig csendesen egymásra tevé kezeit, s töredezett hangon rebegé:

- Az óra végire jár...
Az orvosnak igaza volt...
Már nem fáj semmi...
Sötét lesz minden...
Csak fiaim képe világít még...
Ki jössz felém, onnan a sötétbül?
Állj ott!
Homályos alak!
Még mondanivalóm van itt!

Hanem az a nagy, homályos alak, mely előtűnt a sötétségbül, közelített visszaparancsolhatatlanul, s nem várta meg, míg a nagy, hatalmas, kőszívű ember elmondja, mi parancsolnivalója van még ezen a világon, s rátette arcára láthatatlan kezét.
