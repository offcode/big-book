_  = require('lodash')
const { program } = require('commander')
const fs = require('fs')
const lineByLine = require('n-readlines')

const readJson = (file) => {
    const data = fs.readFileSync(file)
    const json = JSON.parse(data)
    return json
}

const writeJson = (file, obj) => {
    fs.writeFileSync(file, JSON.stringify(obj, null, 2))
}

function saveConfig() {
    writeJson('./config.json', config)
}

config = readJson('./config.json')

toInt = (oldV, newV) => {
    return parseInt(oldV)
}

function readLines(file, start, end) {
    let line
    let lines = []
    let n = 0
    const liner = new lineByLine(file)
    while (line = liner.next()) {
        if (start <= n && n < end) {
            lines.push(line.toString())
        }
        n++
        if (n == end) {
            return lines
        }
    }
    return lines
}

/**
 * Remix items keeping `n` consecutive chunks
 */
function remix(xs, n) {
    const len = Math.floor(xs.length / n) + 1
    const count = xs.length % n
    const longer = _.take(xs, count * len)
    const chunkedLonger = _.chunk(longer, len)
    const shorter = _.drop(xs, count * len)
    const chunkedShorter = _.chunk(shorter, len-1)
    const groups = _.concat(chunkedLonger, chunkedShorter)
    const shuffled = _.shuffle(groups)
    return _.flatten(shuffled)
}

program
    .option('-t, --test <num>', "Dev mode", toInt, 99)

program
    .command('config')
    .description('Change it once')
    .option('-c, --chunk-size <words>', 'How many words to practice with', toInt, config.config.chunkSize)
    .option('-b, --block-count <blocks>', 'How many blocks the exercise is broken up to', toInt, config.config.blockCount)
    .option('-p, --path <file>', 'Path to the text file to be used', config.config.filePath)
    .action((cmd) => {
        config.config = {
            "chunkSize": cmd.chunkSize,
            "blockCount": cmd.blockCount,
            "filePath": cmd.path
        }
        saveConfig()
    })
program
    .command('mix')
    .description('Display the mixed text of the exercise')
    .action(() => {
        let lines
        if (_.toArray(config.state.mixedIndexes) == 0) {
            lines = []
            // determine new lineCount
            let wordCount = 0
            let n = 0
            const liner = new lineByLine(config.config.filePath)
            while (rawLine = liner.next()) {
                line = rawLine.toString()
                if (config.state.firstLine <= n) {
                    let words = line.split(' ')
                    if (config.config.chunkSize < wordCount + words.length) {
                        config.state.lastLine = n + 1
                        break
                    }
                    wordCount += words.length
                    lines.push(line)
                }
                n++
            }
            let lineCount = config.state.lastLine - config.state.firstLine
            config.state.mixedIndexes = remix(_.range(lineCount), config.config.blockCount)
            saveConfig()
        }
        else {
            lines = readLines(config.config.filePath, config.state.firstLine, config.state.lastLine + 1)
        }
        config.state.mixedIndexes.forEach((n) => {
            console.log(lines[n])
        })
    })

program
    .command('original')
    .description('Show the original text of the exercise')
    .action(() => {
        lines = readLines(config.config.filePath, config.state.firstLine, config.state.lastLine + 1)
        lines.forEach((line) => {
            console.log(line)
        })
    })

program
    .command('done')
    .description('Finish current exercise, get ready for the next')
    .action(() => {
        console.log('done');
    })

program
    .parse(process.argv)

